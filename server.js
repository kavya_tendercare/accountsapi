var express = require("express");
var bodyParser = require("body-parser");
var jwt = require('jsonwebtoken');
var env = require("./environment");
var erp = require("./erp");
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(3000, function() {
    console.log('Server started');
});

app.post('/integrationAPI', function (req, res) {
    if (req.body.Username === env.client && req.body.Password === env.client + 'api') {
        var user = {username: req.body.Username, password: req.body.Password};
        var token = jwt.sign(user, env.database);
        return res.status(200).send({auth: true, token});
    } else {
        return res.status(403).send({message: 'Invalid credentials'});
    }
});

 app.get("/integrationAPI/support", verifyToken, function (req, res) { erp.support(req, res)});
 app.get("/integrationAPI/Accounts/Invoices", verifyToken, function (req, res) { erp.invoices(req, res) });
 app.get("/integrationAPI/Accounts/Receipts", verifyToken, function (req, res) { erp.receipts(req, res) });
 app.get("/integrationAPI/Accounts/Refunds", verifyToken, function (req, res) { erp.refunds(req, res) });
 app.get("/integrationAPI/Accounts/CreditNotes", verifyToken, function (req, res) { erp.creditnotes(req, res) });

 app.get("/integrationAPI/Patients/Registration", verifyToken, function (req, res) { erp.patientdata(req, res) });

 app.get("/integrationAPI/Appointments/PatientApptHistory", verifyToken, function (req, res) { erp.patientApptHistory(req, res) });
 app.get("/integrationAPI/Appointments/DoctorSchedule", verifyToken, function (req, res) { erp.doctorSchedule(req, res) });

function verifyToken(req, res, next) {
    var token = req.headers["x-access-token"];
    if (!token) { return res.status(500).send({ auth: false, message: "No token provided" }); }
    jwt.verify(token, env.database, function(err, decoded) {
        if (decoded.username === env.client && decoded.password === env.client + 'api') {
            next();
        } else {
            return res.status(500).send({ auth: false, message: 'Invalid token'});
        }
    });
}