var mysql = require("mysql");
var env = require('./environment');
var options = {
    timeZone: "Asia/Dubai",
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

var formatter = new Intl.DateTimeFormat([], options);
var dtFrom, dtTo
var connection = mysql.createConnection({
    host: env.capsulehost,
    user: env.capsuleuser,
    password: env.capsulepassword,
    database: env.capsuledb,
    port: env.capsuleport
});

connection.connect(function (err) {
    if (err) throw err;
    console.log('Connected');
});


module.exports = {
    support: function (req, res) {
        res.send({ 'Message': 'ERP Success' });
    },
    patientdata: function (req, res) {
        var query = " Select Regn_No as RegNo,Name,DOB,Sex,Mobile,Email,Nationality,Allergies,Comments,Balance,Photo,Active as Status  from patmas";
        if (req.query.RegNo) {
            query += "  where Regn_No= '" + req.query.RegNo + "'"; 
        }
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                if (rows.length) {
                    for (var i = 0; i < rows.length; i++) {
                        rows[i].Photo = env.imageurl+rows[i].Photo;
                        if (rows[i].Status) {
                            rows[i].Status = 'Active';
                        } else {
                            rows[i].Status = 'Inactive';
                        }
                     }
                    res.status(200).json({ "Error": false, 'Total': rows.length, 'Patients': rows });
                    // for (var i = 0; i < rows.length; i++) {
                    //    var query2 = "UPDATE patmas SET CustomField1Show=1 WHERE Regn_No=" + rows[i].RegNo;
                    //     connection.query(query2, function (err, rows) {
                    //         if (err) {
                    //             res.json({ "Error": true, "Message": err.sqlMessage });
                    //         } else {
                    //         }
                    //     });
                    // }
                } else {
                    res.json({ "Error": false, "Message": "No patient with this Registation number" });

                }

            }
        });
       
    },
    patientApptHistory: function (req, res) {
        this.validateRegnNo(req.query.RegNo, res, function (err, validreg) {
            if (validreg) {
                var query = "SELECT appoint.ID,appoint.Oper,appoint.DrName,appoint.AppTime,appoint.Upto,appoint.Regn_no as RegNo,appoint.Name,appoint.Phone,appoint.Purpose,appoint.Remarks,appointstatus.Description,appoint.Room,appoint.Cancelled FROM appoint JOIN appointstatus ON appoint.STATUS=appointstatus.ID WHERE Regn_No='" + req.query.RegNo + "'"
                if (req.query.DateFrom && req.query.DateTo) {
                    query += " and AppTime >='" + req.query.DateFrom + " 00:00:00' AND AppTime<='" + req.query.DateTo + " 23:59:59'";
                } else if (req.query.DateFrom) {
                    query += "  and AppTime >='" + req.query.DateFrom + " 00:00:00'";
                } else if (req.query.DateTo) {
                    query += "  and AppTime<='" + req.query.DateTo + " 23:59:59'";
                } else {
                }
                if (!req.query.include_cancelled) {
                    query += " and cancelled = 0"
                }
                query += " order by apptime";
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'Total': rows.length, 'RegNo': req.query.RegNo, 'PatientAppointments': rows });
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send Registration number of the patient" });
            }
        });
    },
    doctorSchedule: function (req, res) {
        var self = this;
        this.validate(req.query.DateFrom, req.query.DateTo, res, function (err, valid) {
            if (valid) {
                var query = "SELECT appoint.ID,appoint.Oper,appoint.DrName,appoint.AppTime,appoint.Upto,appoint.Regn_no as RegNo,appoint.Name,appoint.Phone,appoint.Purpose,appoint.Remarks,appointstatus.Description as Status,appoint.Room,appoint.Cancelled,patmas.Balance FROM appoint JOIN users on appoint.DrName=users.name join appointstatus on appoint.STATUS=appointstatus.ID join patmas on appoint.Regn_No=patmas.Regn_No WHERE appoint.AppTime >='" + req.query.DateFrom + " 00:00:00' AND AppTime<='" + req.query.DateTo + " 23:59:59'";
                if (req.query.LicenseNo) {
                    query += " and users.License='"+ req.query.LicenseNo +"'"
                }
                if (!req.query.include_cancelled) {
                    query += " and cancelled = 0"
                }
                query += " order by apptime";
                console.log(query);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'Total': rows.length, 'LicenseNo': req.query.LicenseNo, 'DoctorsSchedule': rows });
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send mandatory fields" });
            }
        });
    },
    invoices: function (req, res) {
        this.validate(req.query.DateFrom, req.query.DateTo, res, function (err, valid) {
            if (valid) {
                var query = "SELECT  b.Bill_No,b.OPDBillDate,b.Regn_No,if(p.PayerID IS NULL ,'Self',p.PayerID) AS PayerID, b.Amount AS TotPatAmt,b.InsAmount AS TotInsAmt,b.Discount AS TotDiscount,ROUND((b.Amount+b.InsAmount-b.Net),2) AS TotVat,Consultant,o.SERIAL,o.DepNo,o.Description,o.Rate,o.Qty,ROUND(((o.Rate*o.Qty)-o.Discount-o.Net),2) AS Vat,o.Discount,ROUND((o.Net),2) AS NetItemValue,o.Insurance,b.Operator,b.Cancelled, IF(b.InsID IS NULL,r.activitycode,i.activitycode) AS Activitycode,IF(b.InsID IS NULL,r.rateno,	i.ID) AS Rateno,IF(b.InsID IS NULL,r.Ac_Code,i.Ac_Code) AS AccountCode FROM opdbill as b join opdcharges as o ON b.Bill_No= o.OPD_No LEFT JOIN ratemaster r ON(o.rateno = r.rateno AND b.insID is NULL) LEFT JOIN insurancerate AS i ON(o.rateno = i.ID AND b.insid is NOT NULL) LEFT JOIN patcards as p ON p.ID = b.InsID WHERE date(b.OPDBillDate)>='" + req.query.DateFrom + "' AND date(b.OPDBillDate)<='" + req.query.DateTo + "' AND b.locked <> 1"
                if (!req.query.include_cancelled) {
                    query +=  " and b.cancelled=0"
                }
                var table = [req.query.DateFrom, req.query.DateTo];
                query = mysql.format(query, table);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'Total': rows.length, 'Invoices': rows });
                        for (var i = 0; i < rows.length; i++) {
                            var query2 = "UPDATE opdbill SET locked=1 WHERE bill_no=" + rows[i].Bill_No + " and locked <> 1";
                            connection.query(query2, function (err, rows) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send madatory fields" });
            }
        });


    },
    receipts: function (req, res) {
        this.validate(req.query.DateFrom, req.query.DateTo, res, function (err, valid) {
            if (valid) {
                var query = "SELECT Rect_No,ReceiptsDate,Regn_No,Amount,RectMode,Consultant,Operator,Cancelled  FROM Receipts WHERE date(ReceiptsDate)>='" + req.query.DateFrom + "' AND date(ReceiptsDate)<='" + req.query.DateTo + "' and  locked<>1";
                if (!req.query.include_cancelled) {
                    query += " and cancelled=0";
                }
                var table = [req.query.DateFrom, req.query.DateTo];
                query = mysql.format(query, table);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'Total': rows.length, 'Receipts': rows });
                        for (var i = 0; i < rows.length; i++) {
                            var query2 = "UPDATE receipts SET locked=1 WHERE Rect_No=" + rows[i].Rect_No;
                            connection.query(query2, function (err, rows) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send madatory fields" });
            }
        });
    },

    refunds: function (req, res) {
        this.validate(req.query.DateFrom, req.query.DateTo, res, function (err, valid) {
            if (valid) {
                var query = "SELECT VR_No AS RefundNo,VouchersDate AS RefundDate,Regn_no,Partic as Reason,Amount,Vrmode as RefundMode,Operator,Cancelled FROM vouchers WHERE date(VouchersDate)>='" + req.query.DateFrom + "' AND date(VouchersDate)<='" + req.query.DateTo + "' and CreditNote <> 1 and locked<>1";
                if (!req.query.include_cancelled) {
                    query += " and cancelled=0";
                }
                var table = [req.query.DateFrom, req.query.DateTo];
                query = mysql.format(query, table);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'Refunds': rows });
                        for (var i = 0; i < rows.length; i++) {
                            var query2 = "UPDATE vouchers SET locked=1 WHERE VR_No=" + rows[i].RefundNo;
                            connection.query(query2, function (err, rows) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send madatory fields" });
            }
        });
    },
    creditnotes: function (req, res) {
        this.validate(req.query.DateFrom, req.query.DateTo, res, function (err, valid) {
            if (valid) {
                var query = "SELECT VR_No AS CreditNoteNo,VouchersDate AS CreditNoteDate,Regn_no,Partic as Reason,Amount,Bill_No,Operator,Cancelled FROM vouchers WHERE date(VouchersDate)>='" + req.query.DateFrom + "' AND date(VouchersDate)<='" + req.query.DateTo + "' and CreditNote =1 and locked<>1";
                if (!req.query.include_cancelled) {
                    query += " and cancelled=0";
                }
                var table = [req.query.DateFrom, req.query.DateTo];
                query = mysql.format(query, table);
                connection.query(query, function (err, rows) {
                    if (err) {
                        res.json({ "Error": true, "Message": err.sqlMessage });
                    } else {
                        res.status(200).json({ "Error": false, 'CreditNotes': rows });
                        for (var i = 0; i < rows.length; i++) {
                            var query2 = "UPDATE vouchers SET locked=1 WHERE VR_No=" + rows[i].CreditNoteNo;
                            connection.query(query2, function (err, rows) {
                                if (err) {
                                    res.json({ "Error": true, "Message": err.sqlMessage });
                                } else {
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({ "Error": false, "Message": "Please send madatory fields" });
            }
        });
    },

    validate(from, to, res, valid) {
        if (!from || !to) {
            res.json({ "Error": false, "Message": "Please send mandatory fields" });
        } else {
            if (from > to) {
                res.json({ "Error": false, "Message": "From Date lesser than To Date" });
            } else {
                valid(null, true);
            }
        }
    },
    validateRegnNo(regno, res, validreg) {
        if (!regno) {
            res.json({ "Error": false, "Message": "Please send Registration number of the patient" });
        } else {
            validreg(null, true);
        }
    },
    // validateLicenseNo(license, res, validlicense) {
    //     if (!license) {
    //         res.json({ "Error": false, "Message": "Please send License number of the doctor" });
    //     } else {
    //         validlicense(null, true);
    //     }
    // },
    executeQuery(connection, res, query, message = 'Success', arrName = 'Rows') {
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({ "Error": true, "Message": err.sqlMessage });
            } else {
                res.json({ "Error": false, "Message": message, [arrName]: rows });
            }
        });
    }
}
